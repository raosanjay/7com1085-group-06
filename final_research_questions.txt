								GENERAL RESEARCH QUESTION:

	How emergency services of cosmopolitan cities can be enhanced with the use of current emergency 
	protocols and newer techniques to provide immediate response and quick action, baring in mind of 
	the scenarios of inappropriate use of such services? What features  and modelling would be 
	beneficial for validating the emergent request by learning and performing data analysis on some
	 data fetched from such services?

Ambulance Car:

	The idea is to convert a car into an ambulance in any case of emergency. It aims to help save lives that 
	get no help immediately/ or to the patients who arrive late or rather die on their way to the hospital 
	giving very little time for the doctors to analyse and save a life. Wide application or the thought of 
	implementing this idea came up initially just to get the pregnant women to hospital at the earliest and 
	to cut down the waiting time for the arrival of ambulance. For those cars carrying the patients, have no 
	way to make the traffic aware of the patient in the car, this idea will help them make their way to hospital 
	faster.

	It consists of a central unit, an app, and installation of siren in the car. When there is an emergency, the 
	user/ patient's caretaker contacts the central unit through the app for the approval of using the siren 
	installed on the roof of the car on the road.

	The central unit is responsible for finding out whether the patient is genuine, to help with nearby hospitals, 
	the traffic analysis around the region will help in studying the best path for quick arrival at the medical 
	centres. The nearest hospitals can be tracked and mapped depending on the number of requests coming up from 
	those particular regions.

	App: the application is necessary to provide all the basic and necessary details to the control unit. It 
	might also be helpful to provide images and videos of the patient or even a video call on this app might 
	help to confirm that there is a genuine patient who requires help. It can also serve to track the carís 
	live location until he reaches the hospital.
	Siren: the device installation is to treat a normal car carrying patient to be given way and preference 
	to help them reach the hospital as soon as possible.


1.								SPECIFIC RESEARCH QUESTION:1

	What measures could be taken to control and govern the appropriate use of the vehicle conversion protocol 
	and how will these methodologies and monitoring systems help in data collection and progressing efficiency 
	in data monitoring?

	
	The control procedure can be implemented as how NHS responds to any emergency appointment or sending an ambulance 
	to the patient by asking several questions, to prove the need of convertible emergency vehicle. Also, the central 
	unit takes the car registration number as of one the input, passes it on to the police. So only registered cars on 
	the road are allowed to turn on the siren until they reach the hospital. Incase there are any cars with siren turned
	on without registering with control unit will be liable to pay the penalty for misusing the advantage. Other


								SPECIFIC RESEARCH QUESTION: 2

2. 	What data should be fetched from the requests made for the vehicle conversion and how the data 
	can be trained and analysed for predicting and studying most accident prone areas and providing 
	emergency facilities ?

										CONTEXT:

	The main purpose of studying the data, collected from the requests made by the users, is to 
	know the proper usage of the application. Also, it will help in monitoring the region and the 
	facilities that needs to be installed in some certain regions where needed. The validation 
	and approval would require a thorough monitoring of that region through CCTVs and live video 
	console that the user could enable through the mobile application. 
	
	
										POPULATION:
	
	The Central Unit involved in collecting the data would be networking engineers, 
	data analysts and data scientists. Every specific field will be collecting the data and 
	performing data analysis on the information. The data will contain some certain information like,
	the location of the accident or emergency situation, the presence of any CCTV cameras in that 
	location or the road ways, the nearest medical centres and emergency units.

										INTERVENTION:
	
	The technology required for this software procedure is data collection and data analysis. 
	The chunks of data collected from every request made, will contain some certain information 
	tags that will provide a detailed information regarding the emergent situation. This data will 
	be then studied and trained to perform analysis on possibilites of traffic around the region,
	the presence of CCTVs at the site of incident, the nearest hospitals that could be mapped, etc. 
	
										COMPARISON:
	
	The procedure of the data collection standards shall be the similar process used to collect the
	quantitative data. The main advantage that the intervention procedure provides is that, a user can
	directly give a feedback regarding the location and thorough survey of the incident at real time.

										OUTCOMES:
	
	The outcomes from the data analysis will provide the information regarding the regions that lacks
	basic facilities like CCTVs, proper lighting, etc. Also, the traffic analysis around the region
	will help in studying the best path for quick arrival at the medical centres. The nearest hospitals
	can be tracked and mapped depending on the number of requests coming up from those particular regions.


								SPECIFIC RESEARCH QUESTION: 3

3. 

	How the requests made, to the Central Unit, for converting a vehicle into an ambulance 
	can be verified through some certain protocols for sending a request and what models or factors 
	could be implemented for providing valid approvals and immediate responsiveness?

										CONTEXT
The main motive is to process validation over the requests made during an emergency call. As this
protocol of converting a vehicle into an ambulance could be used for other means, a proper verification
method needs to be implemented. Their are numerous possibilites of using this emergency feature for personal
benefit, to simply avoid the traffic and have a quick drive on roads. So, to avoid such improper usage of the 
service, a genuine modelling is required to grant access to this protocol.


										POPULATION:
To invoke this process, some certain members and developers are required to initiate the working. The very first
team required on this task is the software developers, who need to develop an interface where the unit could monitor
the calls and requests made, by focussing specifically on the CCTVs located at the sight of incident. If not, they
need to provide some other test cases, where the person at incident could provide some valid proof at reat time
to take the benefit of the protocol. Secondly, a team at the unit who would be always making sure that the requests
coming in are genuine and responding evenly and quickly with such requests made.

										INTERVENTION:
Once the unit gets a request from a person who might be in a serious emergency situation, the task force will then try
to map the person's exact location by confirming the location from him/her and then tracking down the CCTV cameras installed
in every corner of that loaction. This step would provide the proof of the incident that has happened for real. In case if
the sight if incident is not covered by any such CCTVs around, or other worst cases, then the unit  can directly access the
cam of the person on call, whether it be the one wounded, or the one who is assessing the person in emergency. This way, 
a thorough validation could be processed.

										OUTCOME:
Through this, a proper verification and validation will be made on the requests that are really meaningful and also, it 
will help in processing all the requests smoothly without entertaining those, which are inappropriate and would simply 
misuse the benefit of the service for quicker progress on such untrue situations. 
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


